var express = require('express');
var app = express();
var baseMLab = 'https://api.mlab.com/api/1/databases/apitechu_inigo/collections/';
var apiKey = 'apiKey=Tu_apx1h-YokfwcIdJvsBW0IlhiINBCJ';
var requestJson = require('request-json');

const bodyParser = require('body-parser');
//app.use(bodyParser.urlencoded());
app.use(bodyParser.json());
var port = process.env.PORT || 3000;
app.listen(port);
console.log("API escuchando en puerto " + port);

app.get("/apitechu/v1", function(req, res){
  console.log("GET /apitechu/v1");
  res.send({"msg":"Hola desde mi portatil"});
});

app.get("/apitechu/v1/users", function(req, res){
  var fs = require('fs');
  console.log("GET /apitechu/v1/users");
  res.writeHead(200, {"Content-Type": "application/json"});
  var json = fs.readFileSync('./usersLogin.json', 'utf8');
  //res.sendFile('users.json', {root: __dirname});
  res.end(json);
});

app.post("/apitechu/v1/users", function(req, res){
  console.log("POST /apitechu/v1/users");
//  console.log(req.headers.first_name);
//  console.log("last_name: " + req.headers.last_name);
//  console.log(req.headers.country);
  console.log("body: "+JSON.stringify(req.body));

/*  var newUser = {
    "first_name" : req.headers.first_name,
    "last_name" : req.headers.last_name,
    "country" : req.headers.country
  }*/
  writeUserDataToFile(req.body);
  res.send("Usuario almacenado");

});

app.post("/apitechu/v1/login", function(req, res){
  console.log("POST /apitechu/v1/login");
  console.log("body: "+JSON.stringify(req.body));
  var fs = require('fs');
  var response = {
    "mensaje":"login incorrecto",
  }
  var jsonArray = JSON.parse(fs.readFileSync('./usersLogin.json', 'utf8'));
  jsonArray.forEach(function(value, id) {
    if(value.email == req.body.email &&
      value.password == req.body.password){
      value.logged = true;
      console.log(JSON.stringify(value));
      response.id = value.id;
      response.mensaje = "login correcto";
    }
  });
  saveUserLoginArray(jsonArray);
  res.send(response);
});

app.post("/apitechu/v1/logout", function(req, res){
  console.log("POST /apitechu/v1/logout");
  console.log("body: "+JSON.stringify(req.body));
  var fs = require('fs');
  var response = {
    "mensaje":"usuario no loggeado",
  }
  var jsonArray = JSON.parse(fs.readFileSync('./usersLogin.json', 'utf8'));
  jsonArray.forEach(function(value, id) {
    if(value.id == req.body.id &&
      value.logged == true){
      value.logged = false;
      console.log(JSON.stringify(value));
      response.mensaje = "usuario deslogeado";
    }
  });
  saveUserLoginArray(jsonArray);
  res.send(response);
});

app.delete("/apitechu/v1/users/:id", function(req, res){
  console.log("DELETE /apitechu/v1/users");
  console.log("user to delete: "+req.params.id);
  var fs = require('fs');
  var jsonArray = JSON.parse(fs.readFileSync('./usersLogin.json', 'utf8'));
  jsonArray.forEach(function(value, id) {
    //console.log(id + " "+ value.id);
    if(value.id == req.params.id){
      jsonArray.splice(id, 1);
    }
  });
  console.log("jsonArray to save: "+jsonArray);
  fs.writeFile('./users.json', JSON.stringify(jsonArray), "utf8", function(err){
    if(err){
      console.log(err);
      return err;
      }else {
      console.log("usuario almacenado");
      return "usuario almacenado";
    }
  });
});

/*app.post("/apitechu/v1/monstruo/:p1/:p2", function(req, resp){
  console.log("POST /apitechu/v1/monstruo/:p1/:p2");
  console.log("params: " );
  console.log(req.params);
  console.log("query:");
  console.log(req.query);
  console.log("body:");
  console.log(req.body);
});*/

app.get("/apitechu/v2/users", function(req, res){
  console.log("GET /apitechu/v2/users");
  var httpClient = requestJson.createClient(baseMLab);
  httpClient.get("user?" + apiKey, function(err, resMLab, body){
    var response = !err ? body : {"msg" : "Error obteniendo usuarios"};
    res.send(response);
  });
});

app.get("/apitechu/v2/users/:id", function(req, res){
  console.log("GET /apitechu/v2/users/:id");

  var id = req.params.id;
  var query = 'q={"id":' + id + '}';
  var httpClient = requestJson.createClient(baseMLab);
  httpClient.get("user?" + query + "&" + apiKey, function(err, resMLab, body){
    //var response = !err ? body : {"msg" : "Error obteniendo usuarios"};
    if(err) {
        response = {"msg" : "Error obteniendo usuarios"};
        res.status(500);
    } else {
      console.log(body);
      if(body.length > 0){
        response = body;
      } else {
        response = {"msg" : "usuario no encontrado"};
        res.status(404);
      }
    }
    res.send(response);
  });
});

app.post("/apitechu/v2/login", function(req, res){
  console.log("POST /apitechu/v1/login");
  var query = 'q={"email":"' + req.body.email + '","password":"' + req.body.password +'"}';
  var httpClient = requestJson.createClient(baseMLab);
  httpClient.get("user?" + query + "&" + apiKey, function(err, resMLab, body){
    if(err) {
        response = {"msg" : "Error: "+err};
        res.status(500);
        res.send(response);
    } else {
      console.log(body);
      if(body.length > 0){
        var putBody = '{"$set":{"logged":true}}';
        httpClient.put("user?" + query + "&" +apiKey, JSON.parse(putBody), function(errPut, resMLabPut, bodyPut){
          if(errPut){
            console.log(errPut);
          }else{
            response = {"msg" : "Bienvenido : " + body[0].first_name + " " + body[0].last_name};
            res.send(response);
          }
        });
            } else {
        response = {"msg" : "usuario no encontrado"};
        res.status(404);
        res.send(response);
      }
    }

  });
});

app.post("/apitechu/v2/logout", function(req, res){
  console.log("POST /apitechu/v2/logout");
  var query = 'q={"id":' + req.body.id + ',"logged":true}';
  var httpClient = requestJson.createClient(baseMLab);
  httpClient.get("user?" + query + "&" + apiKey, function(err, resMLab, body){
    if(err) {
        response = {"msg" : "Error: "+err};
        res.status(500);
        res.send(response);
    } else {
      console.log(body);
      if(body.length > 0){
        var putBody = '{"$set":{"logged":false}}';
        httpClient.put("user?" + query + "&" +apiKey, JSON.parse(putBody), function(errPut, resMLabPut, bodyPut){
          if(errPut){
            console.log(errPut);
          }else{
            response = {"msg" : "Adios, esperamos verte pronto : " + body[0].first_name + " " + body[0].last_name};
            res.send(response);
          }
        });
        } else {
        response = {"msg" : "usuario no logeado"};
        res.status(404);
        res.send(response);
      }
    }

  });
});



function writeUserDataToFile(data){
  var fs = require('fs');
  var jsonArray = JSON.parse(fs.readFileSync('./usersLogin.json', 'utf8'));
  console.log(jsonArray);
  jsonArray.push(data);
  fs.writeFile('./users.json', JSON.stringify(jsonArray), "utf8", function(err){
    if(err){
      console.log(err);
      return err;
      }else {
      console.log("usuario almacenado");
      return "usuario almacenado";
    }
  });
}

function saveUserLoginArray(jsonArray){
  var fs = require('fs');
  console.log("jsonArray to save: "+jsonArray);
  fs.writeFile('./usersLogin.json', JSON.stringify(jsonArray), "utf8", function(err){
    if(err){
      console.log(err);
      }else {
      console.log("cambios guardados");
    }
  });
}
